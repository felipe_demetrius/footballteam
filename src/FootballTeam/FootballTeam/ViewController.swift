//
//  ViewController.swift
//  FootballTeam
//
//  Created by Felipe Silva  on 9/8/15.
//  Copyright (c) 2015 Felipe Silva . All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    var dataSource = [TeamModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupTableView()
        self.loadData()
        
        self.populateDataBase()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func setupTableView(){
        // Setup tableView
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerNib(UINib(nibName: "TeamsTableViewCell", bundle: nil), forCellReuseIdentifier:"teamCell")
        
        self.tableView.rowHeight = 85

    }
    
    func source(){
        for var i = 0 ; i < self.dataSource.count ; i++ {
            println(self.dataSource[i])
        }
    }
    
    
    func loadData(){
        
        TeamRepository.query("", callback: {[weak self] (team: [TeamModel]?) -> () in
            
            if self == nil {
                return
            }
            //println(team)
            //self!.dataSource.append(team!)
            self?.dataSource = team!
            
            self!.tableView.reloadData()
            self!.source()
            })

        
    }
    
    
    func populateDataBase(){
        
        var inter = TeamModel.newInstance()
        inter.name = "Internacional"
        
        var gremio = TeamModel.newInstance()
        gremio.name = "Gremio"

        var vasco = TeamModel.newInstance()
        vasco.name = "Vasco"
        
        var playerss = [PlayerModel]()
        
        var playerss1 = [PlayerModel]()

        var playerss2 = [PlayerModel]()

        
        CoreDataSingleton.shared.save()
        
        for var i = 0 ; i < 10 ; i++ {
            
            
            var play = PlayerModel.newInstance()
            play.name = "Player " + inter.name + " "  + i.description
            play.age = 20 + i
            play.salary = 1000.00 * Float(i)
            //inter.players.addObject(play)
            inter.addPlayersObject(play)
            //play.team = inter
            //playerss.append(play)
            
            
            var supp = SupporterModel.newInstance()
            supp.name = "Supporter " + inter.name + " " + i.description
            supp.registrationId = NSUUID.description()
            supp.overdue = false
            //inter.addSupportersObject(supp)
            inter.supporters.addObject(supp)
            //supp.team = inter
            
            CoreDataSingleton.shared.save()
            
        }

        //inter.players = playerss
        
        
        for var i = 0 ; i < 15 ; i++ {
            var play = PlayerModel.newInstance()
            play.name = "Player " + vasco.name + " "  + i.description
            play.age = 20 + i
            play.salary = 1000.00 * Float(i)
            vasco.addPlayersObject(play)
            //vasco.players.addObject(play)
            //play.team = vasco
            //playerss1.append(play)
            
            //println(vasco.players.count)
            
            var supp = SupporterModel.newInstance()
            supp.name = "Supporter " + vasco.name + " " + i.description
            supp.registrationId = NSUUID.description()
            supp.overdue = false
            vasco.supporters.addObject(supp)
            //supp.team = vasco
            //vasco.addSupportersObject(supp)
        }
        
        //vasco.players = playerss1
        
        for var i = 0 ; i < 8 ; i++ {
            var play = PlayerModel.newInstance()
            play.name = "Player " + gremio.name + " "  + i.description
            play.age = 20 + i
            play.salary = 1000.00 * Float(i)
            gremio.addPlayersObject(play)
            //gremio.players.addObject(play)
            //play.team = gremio
            //playerss2.append(play)
            
            var supp = SupporterModel.newInstance()
            supp.name = "Supporter " + gremio.name + " " + i.description
            supp.registrationId = NSUUID.description()
            supp.overdue = false
            gremio.supporters.addObject(supp)
            //play.team = gremio
            
            //gremio.addSupportersObject(supp)
        }

       // gremio.players = playerss2
        
        CoreDataSingleton.shared.save()
        
        
    
    }

    
    //MARK: table functions
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1 //dataSource.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCellWithIdentifier("teamCell", forIndexPath: indexPath) as! TeamsTableViewCell
        
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //performSegueWithIdentifier("showTeam", sender: dataSource[indexPath.row])
        
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

