//
//  SupporterModel.swift
//  FootballTeam
//
//  Created by Felipe Silva  on 9/8/15.
//  Copyright (c) 2015 Felipe Silva . All rights reserved.
//

import Foundation
import CoreData

@objc(SupporterModel)
class SupporterModel: NSManagedObject {
    
    @NSManaged var registrationId: String
    @NSManaged var name: String
    @NSManaged var overdue: Bool
    @NSManaged var team: TeamModel
    
    class func newInstance() -> SupporterModel {
        var item = NSEntityDescription.insertNewObjectForEntityForName("SupporterModel",
            inManagedObjectContext: CoreDataSingleton.shared.managedObjectContext) as! SupporterModel
        return item
    }

    
}