//
//  TeamModel.swift
//  FootballTeam
//
//  Created by Felipe Silva  on 9/8/15.
//  Copyright (c) 2015 Felipe Silva . All rights reserved.
//

import Foundation
import CoreData

@objc(TeamModel)
class TeamModel: NSManagedObject {
    
    @NSManaged var name: String
    //@NSManaged var players: [PlayerModel]
    @NSManaged var players: NSMutableSet

    @NSManaged var supporters: NSMutableSet
    
    class func newInstance() -> TeamModel {
        var item = NSEntityDescription.insertNewObjectForEntityForName("TeamModel",
            inManagedObjectContext: CoreDataSingleton.shared.managedObjectContext) as! TeamModel
        return item
    }
    
//    func getPlayers() -> [PlayerModel]{
//       return self.players.allObjects as! [PlayerModel]
//    }
    
//    func addPlayers(values: NSSet) {
//        
//        var items = self.mutableSetValueForKey("players");
//        for value in values {
//            items.addObject(value)
//        }
//    }

}

extension TeamModel {
    func addPlayersObject(value: PlayerModel) {
        self.players.addObject(value)
    }
    
    func addSupportersObject(value: SupporterModel) {
        self.supporters.addObject(value)
    }


}