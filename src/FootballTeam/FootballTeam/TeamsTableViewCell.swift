//
//  TeamsTableViewCell.swift
//  FootballTeam
//
//  Created by Felipe Silva  on 9/8/15.
//  Copyright (c) 2015 Felipe Silva . All rights reserved.
//

import UIKit

class TeamsTableViewCell: UITableViewCell {

    @IBOutlet weak var imgTeam: UIImageView!
    
    @IBOutlet weak var lblName: UILabel!
    
    @IBOutlet weak var lblGreastPayroll: UILabel!
    
    @IBOutlet weak var lblSupporters: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
