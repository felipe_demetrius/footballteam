//
//  Player.swift
//  FootballTeam
//
//  Created by Felipe Silva  on 9/8/15.
//  Copyright (c) 2015 Felipe Silva . All rights reserved.
//

import Foundation
import CoreData

@objc(PlayerModel)
class PlayerModel: NSManagedObject {
    
    @NSManaged var age: Int
    @NSManaged var name: String
    @NSManaged var salary: Float
    @NSManaged var team: TeamModel
    
    class func newInstance() -> PlayerModel {
        var item = NSEntityDescription.insertNewObjectForEntityForName("PlayerModel",
            inManagedObjectContext: CoreDataSingleton.shared.managedObjectContext) as! PlayerModel
        
        return item
    }


    
}